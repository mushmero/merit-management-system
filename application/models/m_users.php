<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class M_users extends CI_Model {
	var $usersTable = 'users';
	var $tokenTable = 'tokens';
	var $max_idle = 300;

	function __construct() {
		parent::__construct();
	}

	private function generate_salt($length = 10) {
		$characterList = "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$i = 0;
		$salt = "";
		while ($i < $length) {
			$salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
			$i++;
		}
		return $salt;
	}

	function is_logged_in() {
		$last_activity = $this->session->userdata('last_activity');
		$logged_in = $this->session->userdata('logged_in');
		$user = $this->session->userdata('user');

		if (($logged_in == 'yes')
			&& ((time() - $last_activity) < $this->max_idle)) {
			$this->allow_pass($user);
			return true;
		} else {
			$this->remove_pass();
			return false;
		}
	}

	function checkUser($username) {
		$query = $this->db->get_where($this->usersTable, array('username' => $username), 1);
		if ($query->num_rows() > 0) {
			return $query->row_array();
		}
		return false;
	}

	function hash_password($password) {
		$salt = $this->generate_salt();
		return $salt . '.' . md5($salt . $password);
	}

	function check_password($password, $hashed_password) {
		list($salt, $hash) = explode('.', $hashed_password);
		$hashed2 = $salt . '.' . md5($salt . $password);
		return ($hashed_password == $hashed2);
	}

	function save($user_data) {
		$this->db->insert($this->usersTable, $user_data);
		return $this->db->insert_id();
	}

	function update($user_data) {
		if (isset($user_data['id'])) {
			$this->db->where('id', $user_data['id']);
			$this->db->update($this->usersTable, $user_data);
			return $this->db->affected_rows();
		}
		return false;
	}

	function allow_pass($user_data) {
		$this->session->set_userdata(array('last_activity' => time(), 'logged_in' => 'yes', 'user' => $user_data));
	}

	function remove_pass() {
		/*$array_items = array('last_activity' => ' ', 'logged_in' => ' ', 'user' => ' ');
		$this->session->unset_userdata($array_items);*/
		$this->session->unset_userdata('last_activity');
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('user');
	}

	function get_user_level($username) {
		$this->db->select('level_id');
		$this->db->from($this->usersTable);
		$this->db->where('username', $username);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->level_id;
		} else {
			return false;
		}
	}

	function get_users() {
		$query = $this->db->get($this->usersTable);
		if ($query->num_rows() > 0) {
			return $query->result();
		} else {
			return false;
		}
	}

	function get_user_by_email($email) {
		$this->db->select('id');
		$this->db->from($this->usersTable);
		$this->db->where('email', $email);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			return $query->row()->id;
		} else {
			return false;
		}
	}

	function insertToken($id) {
		$token = substr(sha1(rand()), 0, 30);
		$date = date('Y/m/d');
		$string = array('token' => $token, 'user_id' => $id, 'date_created' => $date);
		$this->db->insert($this->tokenTable, $string);
		return $token . $id;
	}

	function checkToken($token) {
		$tkn = substr($token, 0, 30);
		$uid = substr($token, 30);

		$query = $this->db->get_where($this->tokenTable, array('token' => $tkn, 'user_id' => $uid), 1);

		if ($this->db->affected_rows() > 0) {
			$row = $query->row();
			$dateCreated = $row->date_created;
			$dateCreated1 = strtotime($dateCreated);
			$today = date('Y-m-d');
			$today1 = strtotime($today);
			if ($dateCreated1 != $today1) {
				return false;
			}
			$userInfo = $this->getUserInfo($row->user_id);
			return (object) $userInfo;
		}
		return false;
	}

	function getUserInfo($uid) {
		$query = $this->db->get_where($this->usersTable, array('id' => $uid), 1);
		if ($query->num_rows() > 0) {
			return $query->row_array();
		} else {
			return false;
		}
	}

	function updatePassword($data) {
		/*$this->db->where('id', $newPassword['id']);
			$this->db->update($this->usersTable, array('password' => $newPassword['password']));
		*/
		$query = $this->db->set('password', $data['password'])->where('id', $data['id'])->update($this->usersTable);
		return $this->db->affected_rows();
	}
}