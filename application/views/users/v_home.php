<?php $this->load->view('templates/v_top');?>
<hr>
<div class="container-fluid">
	<div class="row">
			<p>Welcome <strong><?php echo $user['username']; ?></strong>. Member since <?php if ($user['reg_date'] == '') {echo "<strong>none</strong>";} else {echo "<strong>" . date("j/n/Y g:i:s a", $user['reg_date']) . "</strong>";}?>. Click here to <a href="<?php echo site_url('logout'); ?>">Logout</a>.</p>
		<div class="col-xs-1">
			<a class="btn btn-danger" href="<?php echo site_url('add_merit'); ?>"><span class="glyphicon glyphicon-plus-sign"> Add </span></a>
		</div>
	</div>
<hr>
	<div class="row">
		<div class="col-lg-12">
			<p align="center" style="font-size: 30px; font-weight: bold;">My Dashboard</p>
		</div>
	</div>
	<br><br><br>
	<div class="row">
		<div class="col-xs-12 col h  -sm-6 col-md-3">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<th>Recent Points</th>
					</thead>
					<tbody>
						<td>10 pts</td>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<th>Cumulative Points</th>
					</thead>
					<tbody>
						<td>88 pts</td>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<th>Highest Points</th>
					</thead>
					<tbody>
						<td>30 pts</td>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-3">
			<ul class="list-group">
				<li class="list-group-item"><strong>Recent Activities</strong></li>
				<li class="list-group-item">
<?php
foreach ($listname as $ls) {
	$name = $ls->username;
	$pass = $ls->password;
	echo "Name: " . $name . "<br> Password: " . $pass . "<br>";
}
?>
				</li>
				<li class="list-group-item">
				&nbsp;
				</li>
			</ul>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-6">
			<table class="table table-condensed">
				<thead>
					<th>Col 1</th>
					<th>Col 2</th>
					<th>Col 3</th>
				</thead>
				<tbody>
					<td>Inner 1</td>
					<td>Inner 2</td>
					<td>Inner 3</td>
				</tbody>
			</table>
		</div>
		<div class="col-md-6">
			<table class="table table-striped">
				<thead>
					<th>Col 1</th>
					<th>Col 2</th>
					<th>Col 3</th>
				</thead>
				<tbody>
					<td>Inner 1</td>
					<td>Inner 2</td>
					<td>Inner 3</td>
				</tbody>
			</table>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-6">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</div>
		<div class="col-md-6">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</div>
	</div>
</div>
<hr>
<?php $this->load->view('templates/v_bottom');?>
