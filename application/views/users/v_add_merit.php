<?php $this->load->view('templates/v_top');?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4 col-lg-4">&nbsp;</div>
		<div class="col-md-4 col-lg-4">
			<form action="" method="POST" class="form-horizontal">
				<fieldset>
					<legend>
						ADD NEW MERIT
					</legend>
					<div>&nbsp;</div>
					<div class="form-group">
						<label for="" class="control-label col-sm-2">Name: </label>
						<div class="controls col-sm-10">
							<input type="text" class="form-control" readonly value="">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-sm-2">Activity: </label>
						<div class="controls col-sm-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-sm-2">Position: </label>
						<div class="controls col-sm-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="" class="control-label col-sm-2">Date: </label>
						<div class="controls col-sm-10">
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<input type="button" class="btn btn-success btn-lg btn-block" value="Submit">
							<a href="javascript:history.go(-1)" class="btn btn-lg btn-primary btn-block">Back</a>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="col-md-4 col-lg-4">&nbsp;</div>
	</div>
</div>
<?php $this->load->view('templates/v_bottom');?>