<?php $this->load->view('templates/v_top');?>
<div class="container-fluid">
	<div class="row">
	<p>Welcome <strong><?php echo $user['username']; ?></strong>. Member since <?php if ($user['reg_date'] == '') {echo "<strong>none</strong>";} else {echo "<strong>" . date("j/n/Y g:i:s a", $user['reg_date']) . "</strong>";}?>. Click here to <a href="<?php echo site_url('logout'); ?>">Logout</a>.</p>
		<div class="col-lg-12">
			<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</div>
	</div>
</div>
<?php $this->load->view('templates/v_bottom');?>