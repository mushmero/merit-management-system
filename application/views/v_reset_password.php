<?php
$this->load->view('templates/v_top');
$token_error = (trim(form_error('token')) != '') ? ' error' : '';
$password_error = (trim(form_error('password')) != '') ? ' error' : '';
$cpassword_error = (trim(form_error('cpassword')) != '') ? ' error' : '';
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">&nbsp;</div>
		<div class="col-md-4">
		<form class="form-horizontal" method="POST" action="<?php echo site_url() . 'login/reset_password/token/' . $token; ?>">
			<fieldset>
				<legend>Reset Password</legend>
				<div>&nbsp;</div>
				<?php echo (isset($update_error)) ? "<div class=\"alert alert-error\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$update_error</strong></div>" : ''; ?>
				<?php echo (isset($update_success)) ? "<div class=\"alert alert-success\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$update_success</strong></div>" : ''; ?>
				<div class="<?php echo $token_error; ?>">&nbsp;</div>
				<?php if (!isset($update_success)) {?>
				<div class="control-group<?php echo $password_error; ?>">
			      <label class="control-label" for="password">Password</label>
			      <div class="controls">
			        <input type="password" class="form-control" id="password" name="password" value="<?php echo set_value('password'); ?>">
					<?php echo form_error('password', '<p class="help-inline">', '</p>'); ?>
			      </div>
				</div>
			    <div class="control-group<?php echo $cpassword_error; ?>">
			      <label class="control-label" for="cpassword">Confirm Password</label>
			      <div class="controls">
			        <input type="password" class="form-control" id="cpassword" name="cpassword" value="<?php echo set_value('cpassword'); ?>">
					<?php echo form_error('cpassword', '<p class="help-inline">', '</p>'); ?>
			      </div>
				</div>
				<div class="control-group">
			    	<label class="control-label" for="forgot"> </label>
			        <div class="controls">
			    		<input type="submit" value="Reset" class="btn btn-primary btn-lg btn-block">
			        </div>
			    </div>
			    <?php }?>
			</fieldset>
		</form>
		</div>
		<div class="col-md-4">&nbsp;</div>
	</div>
</div>
<?php $this->load->view('templates/v_bottom');?>