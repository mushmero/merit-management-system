<?php
$this->load->view('templates/v_top');
$email_error = (trim(form_error('email')) != '') ? ' error' : '';
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-4">&nbsp;</div>
		<div class="col-md-4">
		<form class="form-horizontal" method="POST" action="<?php echo site_url('forgot'); ?>">
			<fieldset>
				<legend>Forgot Password</legend>
				<p>Please enter your registered email and we'll be get back to you</p>
				<?php echo (isset($reset_button)) ? "<div class=\"alert alert-error\"><button class=\"close\" data-dismiss=\"alert\">&times;</button><strong>$reset_button</strong></div>" : ''; ?>
				<div class="control-group <?php echo $email_error; ?>">
					<label for="email" class="control-label">Email</label>
					<div class="control">
						<input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>">
						<?php echo form_error('email', '<p class="help-inline">', '</p>'); ?>
					</div>
				</div>
				<div class="control-group">
			    	<label class="control-label" for="forgot"> </label>
			        <div class="controls">
			    		<input type="submit" value="Submit" class="btn btn-primary btn-lg btn-block">
			        </div>
			    </div>
			</fieldset>
		</form>
		</div>
		<div class="col-md-4">&nbsp;</div>
	</div>
</div>
<?php $this->load->view('templates/v_bottom');?>