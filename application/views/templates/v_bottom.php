    </div>
  </div>

<?php if (ENVIRONMENT == 'development') {?>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets\js\bootstrap.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets\js\main.js'); ?>"></script>
<?php } else {?>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets\js\main.js'); ?>"></script>
<?php }?>
</body>
</html>
