<?php //$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
<?php
$target_uri = $_SERVER['REQUEST_URI'];
?>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      	</button>
			<a href="<?php if ($this->session->userdata('logged_in') == 'yes') {echo "#";} else {echo base_url();}?>" class="navbar-brand">
				<img class="img-responsive img-brand" src="<?php echo base_url('assets\img\mms.png'); ?>" alt="Home">
			</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav nav-pills" role="navigation">
				<li><a class="active" href="#">Home</a></li>
				<li><a href="#about">About</a></li>
				<li><a href="#contact">Contact</a></li>
			</ul>
			<?php if ($this->session->userdata('logged_in') == 'yes') {?>
			<ul class="nav navbar-nav navbar-right nav-pills" role="navigation">
				<li><p class="navbar-text navbar-right">Welcome <strong><?php echo $user['username']; ?></strong> ! &nbsp;</p></li>
				<li>&nbsp;&nbsp;&nbsp;</li>
				<li><button type="button" class="btn btn-red navbar-btn navbar-right" onclick="location.href='<?php echo site_url('logout'); ?>'">Logout</button></li>
				<li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
			</ul>
			<?php } else {
	?>
			<ul class="nav navbar-nav navbar-right nav-pills" role="navigation">
				<?php if ($target_uri == '/' || $target_uri == '/MMS/') {?>
				<li><button type="button" class="btn navbar-btn btn-red" onclick="location.href='<?php echo site_url('login'); ?>'">Login</button></li>
				<li>&nbsp;</li>
				<li><button type="button" class="btn navbar-btn btn-red" onclick="location.href='<?php echo site_url('register'); ?>'">Register</button></li>
				<li>&nbsp;</li>
				<?php } else if ($target_uri == '/login' || $target_uri == '/MMS/login' || $target_uri == '/logout' || $target_uri == '/login/noaccess') {?>
				<li><button type="button" class="btn navbar-btn btn-red" onclick="location.href='<?php echo site_url('register'); ?>'">Register</button></li>
				<li>&nbsp;</li>
				<?php } else if ($target_uri == '/register' || $target_uri == '/MMS/register') {?>
				<li><button type="button" class="btn navbar-btn btn-red" onclick="location.href='<?php echo site_url('login'); ?>'">Login</button></li>
				<li>&nbsp;</li>
				<?php }?>
			</ul>
			<?php }?>
		</div>
	</div>
</nav>