<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width = device-width, initial-scale = 1">
  <title>MERIT MANAGEMENT SYSTEM</title>
  <link rel="icon" type="image/x-icon" href="<?php echo base_url('assets\img\mms.png'); ?>">
  <?php if (ENVIRONMENT == 'development') {?>
  <link rel="stylesheet" href="<?php echo base_url('assets\css\bootstrap.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets\css\main.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets\css\font-awesome.css'); ?>">
  <?php } else {?>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets\css\main.css'); ?>">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <?php }?>
</head>
<body>
    <?php $this->load->view('templates/v_menu');?>
  <div class="container">
    <div class="row">
		<br><br><br>
    <?php //echo ENVIRONMENT . "<br>" . $_SERVER['REQUEST_URI']; ?>
