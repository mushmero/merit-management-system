<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Login extends CI_Controller {

	var $data = array();

	function __construct() {
		parent::__construct();
		$this->load->model('m_users');
	}

	public function index() {
		if ($this->m_users->is_logged_in()) {
			$user_data = $this->session->userdata('user');
			$username = $user_data['username'];
			if ($this->m_users->get_user_level($username) == 1) {
				redirect('admin');
			} else {
				redirect('users');
			}
		}

		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run()) {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			if ($user = $this->m_users->checkUser($username)) {
				if ($this->m_users->check_password($password, $user['password'])) {
					$this->m_users->allow_pass($user);
					if ($this->m_users->get_user_level($username) == 1) {
						redirect('admin');
					} else {
						redirect('users');
					}
				} else {
					$this->data['login_error'] = 'Invalid username or password';
				}
			} else {
				$this->data['login_error'] = 'Username not found';
			}
		}
		$this->load->view('v_login', $this->data);
	}

	public function register() {
		if ($this->m_users->is_logged_in()) {
			$user_data = $this->session->userdata('user');
			$username = $user_data['username'];
			if ($this->m_users->get_user_level($username) == 1) {
				redirect('admin');
			} else {
				redirect('users');
			}
		}
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('cpassword', 'Password', 'required|matches[password]');

		if ($this->form_validation->run()) {
			$user = array(
				'username' => $this->input->post('username'),
				'password' => $this->m_users->hash_password($this->input->post('password')),
				'email' => $this->input->post('email'),
				'reg_date' => time(),
				'level_id' => 2,
			);
			if ($this->m_users->save($user)) {
				$this->data['register_success'] = 'Registration successful. <a href="' . site_url('login') . '">Click here to login</a>.';
			} else { $this->data['register_error'] = 'Saving data failed. Contact administrator.';}
		}
		$this->load->view('v_register', $this->data);
	}

	public function logout() {
		$this->m_users->remove_pass();
		$this->data['login_success'] = 'You have been logged out. Thank you.';
		$this->load->view('v_login', $this->data);
	}

	public function noaccess() {
		$this->data['login_error'] = 'You do not have access or your login has expired.';
		$this->load->view('v_login', $this->data);
	}

	public function forgot() {
		if ($this->m_users->is_logged_in()) {
			$user_data = $this->session->userdata('user');
			$username = $user_data['username'];
			if ($this->m_users->get_user_level($username) == 1) {
				redirect('admin');
			} else {
				redirect('users');
			}
		}
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		if ($this->form_validation->run()) {
			$email = $this->input->post('email');
			$validEmail = $this->m_users->get_user_by_email($email);
			if (!$validEmail) {
				$this->data['email_error'] = 'Email not found';
			}

			$token = $this->m_users->insertToken($validEmail);
			$encodeToken = $this->base64url_encode($token);
			$url = site_url() . 'login/reset_password/token/' . $encodeToken;
			//$this->data['reset_button'] = 'Click here to <a href="' . $url . '">reset</a>';

			require_once "vendor/autoload.php";

			$credential = array('email' => 'admin@mushmero.com', 'password' => 'admin@mushmero');
			$smtp = array('host' => 'mx1.hostinger.my', 'port' => 587, 'username' => $credential['email'], 'password' => $credential['password'], 'secure' => 'tls');
			$to = $email;
			$subject = 'Reset Password';
			$content = 'To reset your password please click this <a href="' . $url . '"> link</a>.';

			$mail = new PHPMailer;
			$mail->isSMTP();
			$mail->SMTPAuth = true;
			$mail->Host = $smtp['host'];
			$mail->Port = $smtp['port'];
			$mail->Username = $smtp['username'];
			$mail->Password = $smtp['password'];
			$mail->SMTPSecure = $smtp['secure'];
			$mail->From = $credential['email'];
			$mail->FromName = "Administrator";
			$mail->addAddress($to);
			$mail->Subject = $subject;
			$mail->Body = $content;
			$mail->isHTML(true);

			if (!$mail->send()) {
				$this->data['reset_button'] = "Mailer Error: " . $mail->ErrorInfo;
			} else {
				$this->data['reset_button'] = "Message has been sent successfully. Please check your email.";
			}
		}

		$this->load->view('v_forgot', $this->data);
	}

	public function reset_password() {
		$token = $this->base64url_decode($this->uri->segment(4));
		$info = $this->m_users->checkToken($token);
		if (!$info) {
			$this->data['token_error'] = 'Invalid token';
		}
		$this->data = array('token' => $this->base64url_encode($token));
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('cpassword', 'Password', 'required|matches[password]');
		if ($this->form_validation->run()) {
			$newUpdate['password'] = $this->m_users->hash_password($this->input->post('password'));
			$newUpdate['id'] = $info->id;
			if ($this->m_users->updatePassword($newUpdate)) {
				$this->data['update_success'] = 'Password is successfully reset. <a href="' . site_url("login") . '">Click here</a> to login';
			} else {
				$this->data['update_error'] = 'Error resetting password. Please contact admin@mms.mushmero.com';
			}
		}

		$this->load->view('v_reset_password', $this->data);
	}

	public function base64url_encode($data) {
		return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
	}

	public function base64url_decode($data) {
		return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
	}

}