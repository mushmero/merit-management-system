<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	var $data = array();

	function __construct() {
		// Call the Controller constructor
		parent::__construct();
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");

		$this->load->model('m_users');

		if ($this->m_users->is_logged_in() === FALSE) {
			$this->m_users->remove_pass();
			redirect('login/noaccess');
		} else {
			// is_logged_in also put user data in session
			$this->data['user'] = $this->session->userdata('user');
		}
	}
	public function index() {
		$this->data['listname'] = $this->m_users->get_users();
		$this->load->view('users/v_home', $this->data);
	}

	public function add_merit() {
		$this->load->view('users/v_add_merit', $this->data);
	}

}